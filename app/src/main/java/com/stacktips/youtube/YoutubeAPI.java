package com.stacktips.youtube;
/**
 * Created by Scy on 06/01/2017.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

public class YoutubeAPI {
    private YouTube youtube;
    private YouTube.Search.List query;
    private static final long NUMBER_OF_VIDEOS_RETURNED = 50;

    // Replace your Browser Key
    public static final String KEY = "AIzaSyDoWQ_mv8fgjy4u7qTVW0B00YKSoiHS6us";

    public YoutubeAPI(Context context) {
// This object is used to make YouTube Data API requests. The last
// argument is required, but since we don't need anything
// initialized when the HttpRequest is initialized, we override
// the interface and provide a no-op function.

        youtube = new YouTube.Builder(new NetHttpTransport(),
                new JacksonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest hr) throws IOException {
            }
        }).setApplicationName(context.getString(R.string.app_name))
                .build();
        try {

// Define the API request for retrieving search results.
            query = youtube.search().list("id,snippet");

// Set your developer key from the Google Developers Console for
// non-authenticated requests.
            query.setKey(KEY);

// Restrict the search results to only include videos.
// video,channel,playlist.
            query.setType("video");

// To increase efficiency, only retrieve the fields that the
// application uses.
            query.setFields("items(id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url)");

// The maxResults parameter specifies the maximum number of items
// that should be returned in the result set. Acceptable values are
// 0 to 50, inclusive. The default value is 5.
            query.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
// moderate ,none .strict
// query.setSafeSearch("moderate");
// Event Type - completed, live,upcoming
// query.setEventType("completed");
// Location
// query.setLocation("37.42307,-122.08427");
// query.setLocationRadius("5km");
// Oredr-date,rating,relevance,title,videoCount,viewCount
// query.setOrder("rating");
// videoType - any,episode,movie
// query.setVideoType("any");

        } catch (IOException e) {
            Log.d("YoutubeAPI", "Could not initialize: " + e);
        }
    }

    public List<VideoPojo> search(String keywords) {

        query.setQ(keywords);
        try {
// Call the API and print results.
            SearchListResponse response = query.execute();

// Log.e("next page token", response.getNextPageToken());
// Log.e("previous page token", response.getPrevPageToken());
// Log.e("Total Records",
// ""+response.getPageInfo().getTotalResults());
// Log.e("Records per page",
// ""+response.getPageInfo().getResultsPerPage());

            List<SearchResult> results = response.getItems();
            Log.e("YoutubeAPI", "Lenght -" + results.size());

            List<VideoPojo> items = new ArrayList<VideoPojo>();
            for (SearchResult result : results) {
                VideoPojo item = new VideoPojo();
                item.setTitle(result.getSnippet().getTitle());
                item.setDescription(result.getSnippet().getDescription());
                item.setThumbnailURL(result.getSnippet().getThumbnails()
                        .getDefault().getUrl());
                item.setId(result.getId().getVideoId());
                items.add(item);
            }
            return items;
        } catch (IOException e) {
            Log.d("YoutubeAPI", "Could not search: " + e);
            return null;
        }
    }
}