package com.stacktips.youtube;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class YouTubePlayerFragmentActivity extends YouTubeBaseActivity {
    public static final String API_KEY = Config.YOUTUBE_API_KEY;

    public static final String VIDEO_ID = "-m3V8w_7vhk";
    private long StartTime;
    private long EndTime;
    private long totalTime=0;
    private String movieId="https://www.youtube.com/watch?v=";
    private boolean alreadyTimed=false;
    private boolean playedAtLeastOnce=false;
    private boolean alreadyPostedData=false;
    private boolean startBuffer=false;
    private long bufferingStartTime=0;
    private long bufferingEndTime=0;
    private long totalBufferingTime=0;
    private boolean wasPaused=false;

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(!alreadyPostedData) {
            postDataToServer();
            alreadyPostedData=true;
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
        if(!alreadyPostedData) {
            postDataToServer();
            alreadyPostedData=true;
        }
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
        alreadyPostedData=false;
        playedAtLeastOnce=false;
        totalTime=0;
        totalBufferingTime=0;
        StartTime = System.currentTimeMillis();
        //Toast.makeText(this, "Total time: "+TimeUnit.MILLISECONDS.toSeconds(totalTime), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!wasPaused)
        {
            EndTime=System.currentTimeMillis();
        }
        if(!alreadyPostedData) {
            postDataToServer();
            alreadyPostedData=true;
            totalTime=0;
        }
    }

    public void postDataToServer()
    {
        if(!alreadyTimed)
        {
            EndTime=System.currentTimeMillis();
            addTotalTime();
            if(startBuffer)
            {
                bufferingEndTime=System.currentTimeMillis();
                //Toast.makeText(this, "Total time before: "+TimeUnit.MILLISECONDS.toSeconds(totalTime), Toast.LENGTH_LONG).show();
                //Toast.makeText(this, "Buff time: "+TimeUnit.MILLISECONDS.toSeconds(bufferingEndTime - bufferingStartTime), Toast.LENGTH_LONG).show();

                if(bufferingEndTime>bufferingStartTime) {
                    totalBufferingTime += bufferingEndTime - bufferingStartTime;
                }
                totalTime-=totalBufferingTime;
            }

        }
        if(playedAtLeastOnce) {
            Toast.makeText(YouTubePlayerFragmentActivity.this,
                    "Total time: " + TimeUnit.MILLISECONDS.toSeconds(totalTime),
                    Toast.LENGTH_LONG).show();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://channel-recorder-server.herokuapp.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final PostWrapperService service = retrofit.create(PostWrapperService.class);

            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(this.TELEPHONY_SERVICE);
            String identifier = telephonyManager.getDeviceId();

            PostWrapper wPost = new PostWrapper(identifier, Long.toString(TimeUnit.MILLISECONDS.toSeconds(totalTime)), movieId);
            Call<PostWrapper> createCall = service.create(wPost);
            createCall.enqueue(new Callback<PostWrapper>() {
                @Override
                public void onResponse(Call<PostWrapper> _, Response<PostWrapper> resp) {
                    PostWrapper newMov = resp.body();
                }

                @Override
                public void onFailure(Call<PostWrapper> _, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public void addTotalTime()
    {
        if(EndTime-StartTime>0)
        {
            totalTime+=(EndTime-StartTime);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player_fragment);

        FragmentManager fm = getFragmentManager();
        String tag = YouTubePlayerFragment.class.getSimpleName();
        YouTubePlayerFragment playerFragment = (YouTubePlayerFragment) fm.findFragmentByTag(tag);
        if (playerFragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.add(android.R.id.content, playerFragment, tag);
            ft.commit();
        }

        playerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
                movieId+=getIntent().getStringExtra("VIDEO_ID");
                youTubePlayer.cueVideo(getIntent().getStringExtra("VIDEO_ID"));

                // Add listeners to YouTubePlayer instance
                youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                    @Override public void onAdStarted() { }
                    @Override public void onError(YouTubePlayer.ErrorReason arg0) { }
                    @Override public void onLoaded(String arg0) { }
                    @Override public void onLoading() { }
                    @Override public void onVideoEnded() {
                        alreadyTimed=true;
                        EndTime=System.currentTimeMillis();
                        addTotalTime();
                    }
                    @Override public void onVideoStarted() { }
                });


                youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
                    @Override public void onBuffering(boolean arg0) {
                        if(startBuffer && !youTubePlayer.isPlaying())
                        {
                            //Toast.makeText(YouTubePlayerFragmentActivity.this, "End buffering", Toast.LENGTH_SHORT).show();
                            startBuffer=!startBuffer;
                            bufferingEndTime=System.currentTimeMillis();
                            totalBufferingTime+=bufferingEndTime-bufferingStartTime;
                        }
                        else {
                            //Toast.makeText(YouTubePlayerFragmentActivity.this, "Start buffering", Toast.LENGTH_SHORT).show();
                            bufferingStartTime=System.currentTimeMillis();
                            startBuffer=!startBuffer;
                        }
                    }

                    @Override public void onPaused() {
                        wasPaused=true;
                        alreadyTimed=true;
                        EndTime=System.currentTimeMillis();
                        addTotalTime();
                    }
                    @Override public void onPlaying() {
                        wasPaused=false;
                        alreadyTimed=false;
                        alreadyPostedData=false;
                        playedAtLeastOnce=true;
                        StartTime=System.currentTimeMillis();
                    }
                    @Override public void onSeekTo(int arg0) { }
                    @Override public void onStopped() {
                        alreadyTimed=true;
                        EndTime=System.currentTimeMillis();
                        addTotalTime();
                    }
                });
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(YouTubePlayerFragmentActivity.this, "Error while initializing YouTubePlayer.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}