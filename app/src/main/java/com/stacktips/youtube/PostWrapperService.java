package com.stacktips.youtube;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Scy on 07/01/2017.
 */
public interface PostWrapperService {
    @GET("api/watchedMovies/all")
    Call<List<PostWrapper>> all();

    @GET("api/watchedMovies/all/{movieId}")
    Call<PostWrapper> get(@Path("movieId") String movieId);

    @POST("api/watchedMovies")
    Call<PostWrapper> create(@Body PostWrapper pWrap);
}
