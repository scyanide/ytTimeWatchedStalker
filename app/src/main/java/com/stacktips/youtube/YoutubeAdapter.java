package com.stacktips.youtube;
/**
 * Created by Scy on 06/01/2017.
 */

import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

class YoutubeAdapter extends BaseAdapter {
    private List<VideoPojo> list;
    private Context mcontext;
    private LayoutInflater inflater;

    public YoutubeAdapter(Context context, List<VideoPojo> mlist) {
        this.list = mlist;
        this.mcontext = context;
        this.inflater = LayoutInflater.from(mcontext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams") @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item_row, null);

            holder.thumbnail = (ImageView) convertView
                    .findViewById(R.id.video_thumbnail);
            holder.title = (TextView) convertView
                    .findViewById(R.id.video_title);
            holder.description = (TextView) convertView
                    .findViewById(R.id.video_description);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        VideoPojo searchResult = list.get(position);

        Picasso.with(mcontext)
                .load(searchResult.getThumbnailURL()).into(holder.thumbnail);
        holder.title.setText(searchResult.getTitle());
        holder.description.setText(searchResult.getDescription());

        return convertView;
    }

    public class ViewHolder {
        ImageView thumbnail;
        TextView title;
        TextView description;
    }

}
