package com.stacktips.youtube;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Scy on 07/01/2017.
 */
public class PostWrapper {

    @SerializedName("watchingTime")
    String watchingTime;

    @SerializedName("movieId")
    String movieId;

    @SerializedName("identifier")
    String identifier;


    public PostWrapper(String identifier, String watchingTime, String movieId)
    {
        this.movieId=movieId;
        this.watchingTime=watchingTime;
        this.identifier=identifier;
    }

}
