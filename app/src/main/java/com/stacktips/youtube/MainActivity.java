package com.stacktips.youtube;

import java.util.List;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
    private TextView txt_norecordsfound;
    private EditText edt_search;
    private ListView lst_videos;
    private Button btn_search;
    private Handler handler;
    private List<VideoPojo> searchResults;
    private YoutubeAdapter adapter;
    private static final int READ_PHONE_STATE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initComponets();
        addListner();
        handler = new Handler();
        getPermissionsToReadIMEI();

    }

    private void initComponets() {

        txt_norecordsfound = (TextView) findViewById(R.id.txt_norecords);
        edt_search = (EditText) findViewById(R.id.edt_search);
        lst_videos = (ListView) findViewById(R.id.lst_videos);
        btn_search = (Button) findViewById(R.id.btnsearch);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void getPermissionsToReadIMEI() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.READ_PHONE_STATE)) {
                Toast.makeText(this,
                        "We will get your phone IMEI to charge you in future for watching.", Toast.LENGTH_LONG).show();
            }
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                    READ_PHONE_STATE_REQUEST);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == READ_PHONE_STATE_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Read Phone State permission granted", Toast.LENGTH_SHORT).show();
            } else {
                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE);

                if (showRationale) {
                } else {
                    Toast.makeText(this, "Read Phone State permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void addListner() {

        lst_videos
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> av, View v, int pos,
                                            long id) {
                        Intent intent = new Intent(getApplicationContext(),
                                YouTubePlayerFragmentActivity.class);
                        intent.putExtra("VIDEO_ID", searchResults.get(pos)
                                .getId());
                        startActivity(intent);
                    }

                });

        btn_search.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (edt_search.getText().toString().equals("")) {

                } else {
                    new Thread() {
                        public void run() {
                            YoutubeAPI youtubeapi = new YoutubeAPI(
                                    MainActivity.this);

                            searchResults = youtubeapi.search(edt_search
                                    .getText().toString());

                                handler.post(new Runnable() {
                                    public void run() {
                                        if (searchResults != null){
                                            if (searchResults.size() > 0) {
                                                adapter = new YoutubeAdapter(
                                                        MainActivity.this,
                                                        searchResults);
                                                lst_videos.setAdapter(adapter);
                                                txt_norecordsfound
                                                        .setVisibility(View.GONE);
                                            }
                                        }
                                        else {
                                            txt_norecordsfound
                                                    .setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                );
                            }
                    }.start();

                }
            }
        });

    }

}